vim.g.mapleader = " "
vim.g.maplocalleader = " "
vim.opt.signcolumn = "no"
vim.opt.number = true
vim.opt.relativenumber = true
-- cursor always block, no blink
vim.opt.guicursor = "a:block,a:blinkwait0"
vim.opt.wrap = true
vim.opt.expandtab = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.splitright = true
vim.opt.splitbelow = true
-- show a column at 80 characters as a guide for long lines
vim.opt.colorcolumn = "80"
-- sync vim and standard copy register
vim.opt.clipboard = "unnamedplus"
-- cursor always mid-screen
vim.opt.scrolloff = 999
-- visual mode past end of line
vim.opt.virtualedit = "block"
vim.opt.ignorecase = false
-- leader + p for pasting and not losing yanked buffer
vim.keymap.set("v", "<leader>p", "\"_dP")
-- quick save
vim.keymap.set("n", "<leader>w", "<cmd>w<CR>")
-- Ctrl k as ESC
vim.keymap.set({"n", "i", "v", "s", "x", "c", "o", "l", "t"}, "<C-k>", "<C-\\><C-N>")
vim.keymap.set("t", "<Esc>", "<C-\\><C-N>")
-- in and out of terminal mode
vim.keymap.set("n", "<leader>t", "<cmd>te<CR>a")
vim.keymap.set("t", "<C-o>", "<C-\\><C-N><C-o>")
-- [Q]uery [R]eplace in the visually selected area
-- automated search and replace in the selected region
-- leaves you in insert mode for typing: _old_/_new_<CR>
vim.keymap.set("v", "<leader>qr", ":<C-f>is//gcI<Esc>3hi")
-- [G]lobal version for the **EXACT** word under cursor, 
-- leaves you in insert mode for typing the new word
vim.keymap.set("n", "<leader>qr", "yiw:<C-f>i%s///gcI<Esc>5hpla")
-- -- modify search path so it's recursive down (kinda funky)
vim.opt.path = vim.opt.path + "**"
-- find files from vim root
vim.keymap.set("n", "<leader>sf", ":find ")
-- Set highlight on search
vim.o.hlsearch = true
-- [[ Highlight on yank ]]
vim.api.nvim_create_autocmd("TextYankPost", {
  callback = function() vim.highlight.on_yank() end,
  group = vim.api.nvim_create_augroup("YankHighlight", { clear = true }),
  pattern = "*",
})

-- colorscheme ----------------------------------------------------------------
vim.o.termguicolors = true
vim.cmd [[ hi normal guibg=Black ]]
vim.cmd [[ hi Visual guibg=#223249 ]] -- dark blue from Kagawa
vim.cmd [[ hi PmenuSel guibg=Red ]]
vim.cmd [[ hi Pmenu guibg=Indigo ]]
vim.cmd [[ hi CurSearch guibg=Red ]]
vim.cmd [[ hi Search guibg=LightBlue ]]
vim.cmd [[ hi StatusLine cterm=reverse gui=reverse ]]
vim.cmd [[ set pumheight=6 ]] -- how many suggestions
local white_fields = { "@variable", "@variable.parameter", "@variable.parameter.builtin", "@variable.member", "@number", "@number.float", "@operator", "@punctuation.delimiter", "@punctuation.bracket", "@punctuation.special", "LineNrAbove", "LineNr", "LineNrBelow", "@module", "@module.builtin", "@constant", "@constant.macro", }
local kw_fields = { "@keyword", "@keyword.coroutine", "@keyword.function", "@keyword.operator", "@keyword.import", "@keyword.type", "@keyword.modifier", "@keyword.repeat", "@keyword.return", "@keyword.debug", "@keyword.exception", "@keyword.conditional", "@keyword.conditional.ternary", "@keyword.directive", "@keyword.directive.define", "@label", }
local func_fields = { "@attribute", "@attribute.builtin", "@property", "@function", "@function.builtin", "@function.call", "@function.macro", "@function.method", "@function.method.call", "@constructor",}
local string_fields = { "@string", "@string.documentation", "@string.regexp", "@string.escape", "@string.special", "@string.special.symbol", "@string.special.path", "@string.special.url", "@character", "@character.special", "@markup.heading", "@markup.heading.1", "@markup.heading.2", "@markup.heading.3", "@markup.heading.4", "@markup.heading.5", "@markup.heading.6", }
local builtin_fields = { "@variable.builtin", "@constant.builtin", "@boolean", }
local type_fields = { "@type", "@type.builtin", "@type.definition", }

local set_colors = function(fields, color)
  for _, f in ipairs(fields) do vim.api.nvim_set_hl(0, f, {fg=color}) end
end
set_colors(white_fields, "white")
set_colors(kw_fields, "yellow")
set_colors(func_fields, "aqua") -- lightblue, aqua = cyan, darkcyan
set_colors(type_fields, "orange")
set_colors(string_fields, "lime")
set_colors(builtin_fields, "orange")
-- ----------------------------------------------------------------------------

-- set up lazy vim for plugins (~/.local/share/nvim)
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({"git", "clone", "--filter=blob:none", "https://github.com/folke/lazy.nvim.git", "--branch=stable", lazypath,})
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup {
  -- "gc" to comment visual regions/lines
  { "numToStr/Comment.nvim", opts = {} },
  -- automatically close parenthesis when opening
  { "windwp/nvim-autopairs", config = function() require("nvim-autopairs").setup {} end, },
  -- edit and navigate file system 
  {
  "stevearc/oil.nvim",
  config = function ()
    require("oil").setup{
      view_options = {show_hidden = true}, default_file_explorer = true, delete_to_trash = true, use_default_keymaps = false,
      keymaps = {["<CR>"] = "actions.select",},
      vim.keymap.set("n", "-", "<CMD>Oil<CR>", { desc = "Open parent directory" }),
    }
  end,
  },

  -- Highlight, edit, and navigate code
  {
  "nvim-treesitter/nvim-treesitter",
  build = ":TSUpdate",
  config = function()
    require("nvim-treesitter.configs").setup {
      ensure_installed = {
        "c", "lua", "vimdoc", "vim", "query",
        "python", "markdown", "rust", "sql", "go"
      },
      auto_install = false, highlight = { enable = true }, indent = { enable = true },
      textobjects = { select = {
        enable = true, lookahead = true,
        keymaps = {
          ["if"] = "@function.inner", ["af"] = "@function.outer",
          ["ic"] = "@class.inner", ["ac"] = "@class.outer",
        },
      }, },
    }
  end,
  },
  {"https://github.com/nvim-treesitter/nvim-treesitter-textobjects"},

  -- Autocompletion
  {
  "hrsh7th/nvim-cmp",
  event = "InsertEnter",
  dependencies = { "L3MON4D3/LuaSnip", "hrsh7th/cmp-nvim-lsp", "saadparwaiz1/cmp_luasnip", },
  config = function()
    local luasnip = require("luasnip")
    luasnip.config.setup {}
    local cmp = require("cmp")
    cmp.setup {
      snippet = { expand = function(args) luasnip.lsp_expand(args.body) end, },
      completion = { completeopt = "menu,noinsert" },
      mapping = cmp.mapping.preset.insert {
        ["<C-n>"] = cmp.mapping.select_next_item(), ["<C-p>"] = cmp.mapping.select_prev_item(),
        ["<C-y>"] = cmp.mapping.confirm { select = true }, ["<CR>"] = cmp.mapping.confirm { select = true },
      },
      sources = { {name = "luasnip"}, {name = "path"}, {name = "buffer"},
        {name = "nvim_lsp"},
      },
    }
    -- your own snippets here
    local fmt = require("luasnip.extras.fmt").fmt
    luasnip.add_snippets("go", {luasnip.snippet("if",
      fmt([[
        if err != nil {{
            {}log.Fatalln(err)
        }}
      ]],
      { luasnip.insert_node(0), }
    )), })
    luasnip.add_snippets("c", {luasnip.snippet("for",
      fmt([[
        for (size_t i = 0; i < n; ++i) {{
            {}
        }}
      ]],
      { luasnip.insert_node(0), }
    )), })
  end,
  },

  -- LSP Configuration & Plugins
  {
  "neovim/nvim-lspconfig",
  dependencies = { "williamboman/mason.nvim", "williamboman/mason-lspconfig.nvim", "WhoIsSethDaniel/mason-tool-installer.nvim", },
  config = function()
    -- local builtin = require("telescope.builtin")
    vim.api.nvim_create_autocmd("LspAttach", {
      group = vim.api.nvim_create_augroup("kickstart-lsp-attach", { clear = true }),
      callback = function(event)
        vim.keymap.set("n", "K", vim.lsp.buf.hover, { buffer = event.buf, desc = "LSP: Hover Documentation" })
        vim.keymap.set("n", "<leader>rn", vim.lsp.buf.rename, { buffer = event.buf, desc = "LSP: [R]e[n]ame" })
        vim.keymap.set('n','gd', vim.lsp.buf.definition, { buffer = event.buf, desc = "LSP: [G]oto [D]efinition" })
      end,
    })
    local servers = {
      pyright = {
        disableLanguageServices = false, openFilesOnly = true, autoImportCompletion = false,
        python = { analysis = {
            autoImportCompletions = false, autoSearchPaths = false, diagnosticMode = "openFilesOnly",
            useLibraryCodeForTypes = true, typeCheckingMode = "basic" -- off, basic, strict
        }, },
      },
      rust_analyzer = {},
      clangd = {},
      gopls = {},
    }
    require("mason").setup()
    local ensure_installed = vim.tbl_keys(servers or {})
    require("mason-tool-installer").setup {ensure_installed = ensure_installed,}
    local capabilities = vim.lsp.protocol.make_client_capabilities()
    capabilities = vim.tbl_deep_extend("force", capabilities, require("cmp_nvim_lsp").default_capabilities())
    require("mason-lspconfig").setup {
      handlers = {
        function(server_name)
          local server = servers[server_name] or {}
          server.capabilities = vim.tbl_deep_extend("force", {}, capabilities, server.capabilities or {})
          -- disable semantic tokens to avoid color changes
          server.on_attach = function (client)
            client.server_capabilities.semanticTokensProvider = nil
          end
          require("lspconfig")[server_name].setup(server)
        end,
      },
    }
  end,
  },
}
