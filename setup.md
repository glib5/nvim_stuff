
# .bashrc
(ll alias, basic shell prompt with newline)

- alias ls='ls --color=auto'
- alias ll='ls -alFh --time-style long-iso --color=auto'

- PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\n$ '

# remap caps lock to esc (keyboard settings)
in /etc/default/keyboard, sudo vi and add this line

**XKBOPTIONS="caps:escape"**

# window manager mapping
(h, j, k, l + alt, draggable border width ?)

# sudo apt install
neovim, init.lua (node, npm ?)
pyhton
rust
latex
steam

