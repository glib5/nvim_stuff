vim.g.mapleader = ' '
vim.g.maplocalleader = ' '
vim.opt.number = true
vim.opt.relativenumber = true
-- cursor always block, no blink
vim.opt.guicursor = "a:block,a:blinkwait0"
vim.opt.wrap = true
vim.opt.expandtab = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
-- sync vim and standard copy registry
vim.opt.clipboard = "unnamedplus"
-- cursor always mid-screen
vim.opt.scrolloff = 999
-- visual mode past end of line
vim.opt.virtualedit = "block"
vim.opt.ignorecase = true

-- Ctrl k as ESC
vim.keymap.set('n', '<C-k>', '<Esc>')
vim.keymap.set('i', '<C-k>', '<Esc>')
vim.keymap.set('v', '<C-k>', '<Esc>')
vim.keymap.set('s', '<C-k>', '<Esc>')
vim.keymap.set('x', '<C-k>', '<Esc>')
vim.keymap.set('c', '<C-k>', '<Esc>')
vim.keymap.set('o', '<C-k>', '<Esc>')
vim.keymap.set('l', '<C-k>', '<Esc>')
vim.keymap.set('t', '<C-k>', '<C-\\><C-N>')
vim.keymap.set('t', '<Esc>', '<C-\\><C-N>')

vim.opt.splitright = true
vim.opt.splitbelow = true

-- leader + p for pasting and not losing yanked buffer
vim.keymap.set("v", "<leader>p", "\"_dP")

-- quick save
vim.keymap.set('n', '<leader>w', '<cmd>w<cr>')

-- show a column at 80 characters as a guide for long lines
vim.opt.colorcolumn = '80'
-- except in Rust where the rule is 100 characters
vim.api.nvim_create_autocmd('Filetype', { pattern = 'rust', command = 'set colorcolumn=100' })

-- Set highlight on search
vim.o.hlsearch = true

-- [[ Highlight on yank ]]
-- See `:help vim.highlight.on_yank()`
vim.api.nvim_create_autocmd('TextYankPost', {
  callback = function()
    vim.highlight.on_yank()
  end,
  group = vim.api.nvim_create_augroup('YankHighlight', { clear = true }),
  pattern = '*',
})

-- modify search path so it's recursive down
vim.opt.path = vim.opt.path + "**"
-- find files from vim root
vim.keymap.set('n', '<leader>sf', ':find ')

-- set up lazy vim for plugins
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable",
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require('lazy').setup {

  -- "gc" to comment visual regions/lines
  { 'numToStr/Comment.nvim', opts = {} },

  -- Fuzzy Finder (files, lsp, etc)
  -- {
  --   'nvim-telescope/telescope.nvim',
  --   -- event = 'VimEnter',
  --   branch = '0.1.x',
  --   dependencies = {
  --     'nvim-lua/plenary.nvim',
  --     { 'nvim-telescope/telescope-fzf-native.nvim', build = 'make'}
  --   },
  --   config = function()
  --     require('telescope').setup {
  --       pickers = {find_files = {theme = "ivy"}},
  --       extensions = {fzf = {}}
  --     }
  --     require('telescope').load_extension('fzf')
  --     local builtin = require 'telescope.builtin'
  --     vim.keymap.set('n', '<leader>sf', builtin.find_files, { desc = '[S]earch [F]iles' })
  --   end,
  -- },

  -- theme, comes in 3 flavors: wave, dragon, lotus
  {
    "rebelot/kanagawa.nvim",
    config = function()
      -- theme but black background
      vim.api.nvim_command([[
          augroup ChangeBackgroudColour
              autocmd colorscheme * :hi normal guibg=#0a0a0a
          augroup END
      ]])
      vim.o.termguicolors = true
      vim.cmd [[silent! colorscheme kanagawa-wave]]
      -- white line numbers
      vim.api.nvim_set_hl(0, 'LineNrAbove', { fg='white' })
      vim.api.nvim_set_hl(0, 'LineNr', { fg='white' })
      vim.api.nvim_set_hl(0, 'LineNrBelow', { fg='white' })
    end,
  },

  -- Highlight, edit, and navigate code
  {
    'nvim-treesitter/nvim-treesitter',
    build = ':TSUpdate',
    config = function()
      ---@diagnostic disable-next-line: missing-fields
      require('nvim-treesitter.configs').setup {
        ensure_installed = {
          'c',
          'lua',
          'python',
          'markdown',
          'rust',
          'vimdoc',
          'vim',
          'javascript',
          'go'
        },
        auto_install = false,
        highlight = { enable = true },
        indent = { enable = true },
      }
    end,
  },

  -- automatically close parenthesis when opening
  {
    "windwp/nvim-autopairs",
    dependencies = { 'hrsh7th/nvim-cmp' },
    config = function() require("nvim-autopairs").setup {} end,
  },

  -- Set lualine as statusline, -- See `:help lualine.txt`
  {
    'nvim-lualine/lualine.nvim',
    opts = {
      options = {
        icons_enabled = false,
        component_separators = '|',
        section_separators = '',
      },
      sections = {
        -- show full path of files
        lualine_c = {{"filename", path=2}}
      },
    },
  },

  -- edit and navigate file system 
  {
    'stevearc/oil.nvim',
    config = function ()
      require('oil').setup{
        view_options = {show_hidden = true},
        default_file_explorer = true,
        delete_to_trash = true,
        use_default_keymaps = false,
        keymaps = {["<CR>"] = "actions.select",},
        vim.keymap.set("n", "-", "<CMD>Oil<CR>", { desc = "Open parent directory" }),
      }
    end,
  },

  -- LSP Configuration & Plugins
  -- {
  --   'neovim/nvim-lspconfig',
  --   dependencies = {
  --     'williamboman/mason.nvim',
  --     'williamboman/mason-lspconfig.nvim',
  --     'WhoIsSethDaniel/mason-tool-installer.nvim',
  --     { 'j-hui/fidget.nvim', opts = {} },
  --   },
  --   config = function()
  --     vim.api.nvim_create_autocmd('LspAttach', {
  --       group = vim.api.nvim_create_augroup('kickstart-lsp-attach', { clear = true }),
  --       callback = function(event)
  --         local normal_mode_keymap = function(keys, func, desc)
  --           vim.keymap.set('n', keys, func, { buffer = event.buf, desc = 'LSP: ' .. desc })
  --         end
  --         normal_mode_keymap('K', vim.lsp.buf.hover, 'Hover Documentation')
  --         normal_mode_keymap('gd', require('telescope.builtin').lsp_definitions, '[G]oto [D]efinition')
  --         normal_mode_keymap('gr', require('telescope.builtin').lsp_references, '[G]oto [R]eferences')
  --         normal_mode_keymap('<leader>ds', require('telescope.builtin').lsp_document_symbols, '[D]ocument [S]ymbols')
  --         -- normal_mode_keymap('<leader>ws', require('telescope.builtin').lsp_dynamic_workspace_symbols, '[W]orkspace [S]ymbols')
  --         normal_mode_keymap('<leader>rn', vim.lsp.buf.rename, '[R]e[n]ame')
  --         -- normal_mode_keymap('<leader>ca', vim.lsp.buf.code_action, '[C]ode [A]ction')
  --       end,
  --     })
  --     local capabilities = vim.lsp.protocol.make_client_capabilities()
  --     capabilities = vim.tbl_deep_extend('force', capabilities, require('cmp_nvim_lsp').default_capabilities())
  --     local servers = {
  --       -- lua_ls = {
  --       --   settings = {
  --       --     Lua = {
  --       --       runtime = { version = 'LuaJIT' },
  --       --       diagnostics = { globals = {'vim','require'}, },
  --       --       workspace = {
  --       --         -- Make the server aware of Neovim runtime files
  --       --         library = vim.api.nvim_get_runtime_file("", true),
  --       --         checkThirdParty = false,
  --       --       },
  --       --       completion = {callSnippet = 'Replace',},
  --       --     },
  --       --   },
  --       -- },
  --       pyright = {
  --         disableLanguageServices = false,
  --         openFilesOnly = true,
  --         autoImportCompletion = false,
  --         python = {
  --           analysis = {
  --             autoImportCompletions = false,
  --             autoSearchPaths = false,
  --             diagnosticMode = 'openFilesOnly',
  --             useLibraryCodeForTypes = true,
  --             typeCheckingMode = 'basic' -- off, basic, strict
  --           },
  --         },
  --       },
  --       rust_analyzer = {},
  --       clangd = {},
  --     }
  --     require('mason').setup()
  --     local ensure_installed = vim.tbl_keys(servers or {})
  --     require('mason-tool-installer').setup {ensure_installed = ensure_installed,}
  --     require('mason-lspconfig').setup {
  --       handlers = {
  --         function(server_name)
  --           local server = servers[server_name] or {}
  --           server.capabilities = vim.tbl_deep_extend('force', {}, capabilities, server.capabilities or {})
  --           require('lspconfig')[server_name].setup(server)
  --         end,
  --       },
  --     }
  --   end,
  -- },

  -- Autocompletion
  -- {
  --   'hrsh7th/nvim-cmp',
  --   event = 'InsertEnter',
  --   dependencies = {
  --     {'L3MON4D3/LuaSnip',},
  --     'saadparwaiz1/cmp_luasnip',
  --     'hrsh7th/cmp-nvim-lsp',
  --     'hrsh7th/cmp-path',
  --     'rafamadriz/friendly-snippets',
  --   },
  --   config = function()
  --     local luasnip = require 'luasnip'
  --     luasnip.config.setup {}
  --     -- your own snippets here
  --     -- luasnip.add_snippets(
  --     --   "lua",
  --     -- {luasnip.snippet("hello", {luasnip.text_node('print("hello")')}),}
  --     -- )
  --     -- html auto tags
  --     require("luasnip.loaders.from_vscode").load {
  --       exclude = {"javascript", "txt"},
  --       include = {"html"},
  --     }
  --     local cmp = require 'cmp'
  --     cmp.setup {
  --       snippet = {
  --         expand = function(args)
  --           luasnip.lsp_expand(args.body)
  --         end,
  --       },
  --       completion = { completeopt = 'menu,menuone,noinsert' },
  --       mapping = cmp.mapping.preset.insert {
  --         -- Select the [n]ext item
  --         ['<C-n>'] = cmp.mapping.select_next_item(),
  --         -- Select the [p]revious item
  --         ['<C-p>'] = cmp.mapping.select_prev_item(),
  --         -- confirm choice
  --         ['<C-y>'] = cmp.mapping.confirm { select = false },
  --         ['<CR>'] = cmp.mapping.confirm { select = true },
  --       },
  --       sources = {
  --         { name = 'nvim_lsp' },
  --         { name = 'luasnip' },
  --         { name = 'path' },
  --       },
  --     }
  --   end,
  -- },

  -- -- indentation guide
  -- {
  --   "lukas-reineke/indent-blankline.nvim",
  --   -- main = "ibl",
  --   -- ---@module "ibl"
  --   -- ---@type ibl.config
  --   -- opts = {},
  --   config = function ()
  --     require('ibl').setup({
  --       -- just the vertical lines please
  --       scope = {enabled = false},
  --       -- and very thin as well 
  --       indent = {char = "▏"},
  --     })
  --   end
  -- }
}


-- -- open terminal in a floating window
-- -- https://www.youtube.com/watch?v=5PIiKDES_wc&list=WL&index=3
-- local state = {
--   floating = {
--     buf = -1,
--     win = -1,
--   }
-- }
-- local function create_floating_window(opts)
--   opts = opts or {}
--   local width = opts.width or math.floor(vim.o.columns * 0.6)
--   local height = opts.height or math.floor(vim.o.lines * 0.8)
--   -- Calculate the position to center the window
--   -- local col = math.floor((vim.o.columns - width) / 2)
--   -- local row = math.floor((vim.o.lines - height) / 2)
--   -- Calculate the position to place the window on the right
--   local col = math.floor((vim.o.columns - width) / 1)
--   local row = math.floor((vim.o.lines - height) / 2)
--   -- Create a buffer
--   local buf = nil
--   if vim.api.nvim_buf_is_valid(opts.buf) then
--     buf = opts.buf
--   else
--     buf = vim.api.nvim_create_buf(false, true) -- No file, scratch buffer
--   end
--   -- Define window configuration
--   local win_config = {
--     relative = "editor",
--     width = width,
--     height = height,
--     col = col,
--     row = row,
--     style = "minimal", -- No borders or extra UI elements
--     border = "rounded",
--   }
--   -- Create the floating window
--   local win = vim.api.nvim_open_win(buf, true, win_config)
--   return { buf = buf, win = win }
-- end
-- local toggle_terminal = function()
--   if not vim.api.nvim_win_is_valid(state.floating.win) then
--     state.floating = create_floating_window { buf = state.floating.buf }
--     if vim.bo[state.floating.buf].buftype ~= "terminal" then
--       vim.cmd.terminal()
--     end
--   else
--     vim.api.nvim_win_hide(state.floating.win)
--   end
-- end
-- -- Create a floating window with default dimensions
-- -- vim.api.nvim_create_user_command("Floaterminal", toggle_terminal, {})
-- vim.keymap.set('n', '<leader>t', toggle_terminal)

-- HOME MADE SNIPPETS
-- read some file and paste its content at cursor location
-- vim.keymap.set('n', '<leader>pym', '<cmd>-1read $HOME/.config/nvim/pymain.py<cr>')
