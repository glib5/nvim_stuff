# nvim_stuff

minimal collection of resources to get started with Neovim 

## includes
- info on how to get started on windows
- a **very** minimal configuration
- links to better resources

## How to install neovim on windows

- Install 
 - Neovim
 - Git
 - Visual Studio Installer
 - LLVM to get access to clang (follow this video)
  - https://www.youtube.com/watch?v=u98Mh8qLIjo
- init.lua goes here: 
  - C:\Users\USERNAME\AppData\Local\nvim
- Follow this video for writing your own init.lua file:
  - https://www.youtube.com/watch?v=N-RFCfs6rxI
- alternatively, here's nvim kickstart:  
  - https://github.com/nvim-lua/kickstart.nvim/blob/master/init.lua
- for **python lsp (pyright)** you'll need node and npm (LTS version)
  - https://nodejs.org/en/download/
	- node -v, npm -v
	- npm i -g pyright
- remap caps lock to esc: https://www.youtube.com/watch?v=PlPoG7MAt_g

